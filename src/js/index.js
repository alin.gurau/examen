//import jquery
var $ = require("jquery");
import "bootstrap";
import "../style/scss/style.scss";

$(document).ready(function () {
    console.log("TEST")
})

$(document).ready(function () {
    $(".dropdown-toggle").dropdown();
});

$(document).ready(function () {
    $.getJSON('https://baconipsum.com/api/?type=all-meat&sentences=1&start-with-lorem=1',
            { 'type': 'meat-and-filler', 'start-with-lorem': '1', 'paras': '3' },
            function (baconGoodness) {
                if (baconGoodness && baconGoodness.length > 0) {
                    $("#loremIpsum1").html("");
                    for (var i = 0; i < baconGoodness.length; i++)
                        $("#loremIpsum1").append(
                          "<p>" +
                            baconGoodness[i] +
                            "</p>"
                        );
                    $("#loremIpsum1").show();
                }
            });
});

$(document).ready(function () {
    $.getJSON(
      "https://baconipsum.com/api/?type=all-meat&paras=2&start-with-lorem=1",
      { type: "meat-and-filler", "start-with-lorem": "1", paras: "3" },
      function(baconGoodness) {
        if (baconGoodness && baconGoodness.length > 0) {
          $("#cardLoremIpsum").html("");
          for (var i = 0; i < baconGoodness.length; i++)
              $("#cardLoremIpsum").append("<p>" + baconGoodness[i] + "</p>");
            $("#cardLoremIpsum").show();
        }
      }
    );
});

$(document).ready(function () {
    $("#baconButton").click(function () {
        $.getJSON('https://baconipsum.com/api/?callback=?',
            { 'type': 'meat-and-filler', 'start-with-lorem': '1', 'paras': '3' },
            function (baconGoodness) {
                if (baconGoodness && baconGoodness.length > 0) {
                    $("#baconIpsumOutput").html('');
                    for (var i = 0; i < baconGoodness.length; i++)
                        $("#baconIpsumOutput").append('<p>' + baconGoodness[i] + '</p>');
                    $("#baconIpsumOutput").show();
                }
            });
    });
});
